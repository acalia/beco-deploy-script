## This repo contains scripts for deploying Java servers on operational BECO machines

Just clone the repo and you'll be able to run the `deploy_product.sh`.

A simple example is:
```
./deploy_product.sh -p lhc-coupling-server -a coupling-server
```