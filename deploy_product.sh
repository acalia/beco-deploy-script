#!/bin/bash

ask_confirmation=true
deploy_dev=false
product_name=
app_name=
use_prometheus=false
prometheus_port=

usage()
{
    echo "usage: deploy_product <-p | --product-name> <-a | --app-name> [-prometheus-port] [-dev] [-no-confirmation]"
}

while [ "$1" != "" ]; do
    case $1 in
        -p | --product-name )   product_name=$2
                                shift
                                ;;
        -a | --app-name )       app_name=$2
                                shift
                                ;;
        -dev )                  deploy_dev=true
                                ;;
        -no-confirmation )      ask_confirmation=false
                                ;;
        -prometheus-port )      use_prometheus=true
                                prometheus_port=$2
                                shift
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done

if [ $(whoami) != "copera" ]; then
	echo "You can only deploy by logging as copera. $(whoami) is not allowed to deploy."
	exit 1
fi

if [ -z "$product_name" ]; then
    echo "Product name not specified"
    usage
    exit 1
fi

if [ -z "$app_name" ]; then
    echo "Application name not specified"
    usage
    exit 1
fi

if [ "$ask_confirmation" = "true" ]; then
    echo -n "Do you confirm that you want to deploy $app_name $product_name? (yes/no) > "
    read response
    if [ "$response" != "yes" ]; then
        echo "Exiting without changes"
        exit 1
    fi
fi

echo "You're about to deploy product=$product_name app=$app_name"

app_jvm=`echo $app_name | tr '[a-z]' '[A-Z]'`".jvm"

echo "Stopping app $app_jvm..."
wreboot -kill $app_jvm || exit $?
echo "$app_jvm stopped"

if [ "$deploy_dev" = "true" ]; then
    echo "Deploying DEV version of product=$product_name app=$app_name..."
    deploy $product_name -a $app_name -f --dev  || exit $?
    echo "Done"
else
    echo "Deploying PRO version of product=$product_name app=$app_name..."
    deploy $product_name -a $app_name -f  || exit $?
    echo "Done"
fi

if [ "$use_prometheus" = "true" ]; then
    echo "Instrumenting $app_name with Prometheus on port $prometheus_port..." 
    /user/copera/Deployment/java/addPrometheusAgent.sh /opt/$product_name/$app_name/bin/$app_jvm $prometheus_port
fi

echo "Restarting $app_jvm..."
wreboot -n $app_jvm  || exit $?
echo "$app_jvm restarted"

read -p "Press ENTER to see the application logs, you can exit safely with Ctrl+C"

cd /opt/$product_name/$app_name/log/
find . -cmin 1 -name "*.log" | xargs tail -F
